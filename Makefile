CC := ghc
BUILD_DIR := build

OUT_EXT := o hi

.PHONY: clean
all: $(BUILD_DIR)/tri

$(BUILD_DIR):
	mkdir -p $@

$(BUILD_DIR)/tri: TriRapide.hs $(BUILD_DIR)
	$(CC) -o $(BUILD_DIR)/$(notdir $@) $<

clean:
	rm -rf $(foreach ext,$(OUT_EXT),TriRapide.$(ext))

